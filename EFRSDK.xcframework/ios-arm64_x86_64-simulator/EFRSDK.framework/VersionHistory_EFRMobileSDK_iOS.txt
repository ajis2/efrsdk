EFR MobileSDK for iOS Version History
=====================================

Ver. 3.6.3          06 June 2021
-------------------------------

Fix:

- Dynamic secret key integration


Ver. 3.6.2          06 June 2021
-------------------------------

Update:

- Added support for iOS 11

- Added compatibility for swift 5.4
 

Ver. 3.6.1          04 June 2021
-------------------------------

Update:

- Added support for simulators


Ver. 3.6.0          03 June 2021
-------------------------------

Update:

- Enhanced challenge and response

- Added new  dynamic secret key hashing mechanism

- Added inmobile face recognition


Ver. 3.5.0          02 June 2021
-------------------------------

Update:

- Added passive liveness checks

Ver. 3.4.0          16 May 2021
-------------------------------

Update:

- Disabled close and open eye liveness checks in older devices

- SDK license expiry date: No Expiry Date

Fix:

- Smile and don't smile : Adjusted threshold value to improve performance in iPhone X and above

- Tilt the head and back to normal: Adjusted threshold value to improve performance


Ver. 3.3.7          28 Apr 2021
-------------------------------

Update:

- SDK license expiry date: No Expiry Date


Ver. 3.3.6          08 Apr 2021
-------------------------------

Update:

- SDK license expiry date: 30th of October 2021


Ver. 3.3.5          02 Mar 2021
-------------------------------

Update:

- Bitcode flag enabled while taking build

- SDK license expiry date: 30th of April 2021


Ver. 3.3.4          03 Nov 2020
-------------------------------

Update:

- SDK license expiry date: 30th of April 2021

Ver. 3.3.3          01 Nov 2020
-------------------------------

Fix:

- Vibration feedback changed to haptic feedbacks in devices running iOS 12.4 or less

Update:

- SDK license expiry date: 30th of December 2020


Ver. 3.3.2          12 Oct 2020
-------------------------------

Fix:

- Application document directory cleaned instead of SDK document directory

Update:

- SDK license expiry date: 30th of October 2020

Ver. 3.3.1          05 Oct 2020
-------------------------------

Fix:

- Portrait image count returned in result class not matching with portrait count set in sdk configuration

- Crash when receiving memory warning while saving multiple portrait images

Update:

- SDK license expiry date: 30th of October 2020

Ver. 3.3.0          23 Jul 2020
-------------------------------
Feature:

- Implemented Intructional Feedback Handler(supplementary) that sends feedback message(s) only on a validated feedback change.

- Liveness checks explicitly would require the user to bring his/her head /face to the neutral position at the end of each liveness check test in order for that check to pass.

- Liveness check SMILE require user to smile then do not smile in order for that check to pass


Updates:

- Description and suggested messages for liveness checks updated

- Portrait image blur detection with MetalKit


Fix:

- Liveness check CLOSE_THEN_OPEN_LEFT_EYE require user close then open left eye in order for that check to pass

- Liveness check LOOK_TO_RIGHT_DIRECTION require user close then open right eye in order for that check to pass

- Liveness check OPEN_THEN_CLOSE_MOUTH require user open then close mouth in order for that check to pass



Ver. 3.2.2          11 Jun 2020
-------------------------------

Fix:
- Crash when running project with EFR MobileSDK in iOS less than 12.4
