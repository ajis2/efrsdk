Pod::Spec.new do |spec|
  spec.name          = 'EFRSDK'
  spec.version       = '3.6.8'
  spec.license       = { :type => 'BSD' }
  spec.homepage      = 'https://gitlab.com/ajis2/efrsdk'
  spec.authors       = { 'AICENTER' => 'ajis@aicenter.ae' }
  spec.summary       = 'ARC and GCD Compatible Reachability Class for iOS and OS X.'
  spec.source        = { :git => 'git@gitlab.com:ajis2/efrsdk.git', :tag => '3.6.8' }
  spec.swift_version = '4.2'
  spec.vendored_frameworks = 'EFRSDK.xcframework'
  spec.ios.deployment_target  = '12.0'
  spec.dependency 'AFNetworking', '~> 4.0'
end
